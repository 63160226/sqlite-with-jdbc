/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.satit.databaseproject.service;

import com.satit.databaseproject.dao.UserDao;
import com.satit.databaseproject.model.User;

/**
 *
 * @author satit
 */
public class Userservice {
    public User login(String name,String password){
        UserDao userDao = new UserDao();
        User user = userDao.getbyName(name);
        if(user != null && user.getPassword().equals(password)){
            return user;
        }
        return null;
    }
}
