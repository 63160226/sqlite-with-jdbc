/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.satit.databaseproject;

import com.satit.databaseproject.model.User;
import com.satit.databaseproject.service.Userservice;

/**
 *
 * @author satit
 */
public class Testuserservice {

    public static void main(String[] args) {
        Userservice userService = new Userservice();
        User user = userService.login("user2", "password");
        if (user != null) {
            System.out.println("Welcome user : " + user.getName());
        } else {
            System.out.println("Error");

        }
    }
}
